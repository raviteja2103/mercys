import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import json
import csv

#Reading from Denials and Writing to a modified file
with open('/Users/raviteja/Documents/workspace/mercy/Ayasdi_Denials.csv','r') as csvinput:
    with open('/Users/raviteja/Documents/workspace/mercy/Ayasdi_Denials_modified.csv', 'w') as csvoutput:
        writer = csv.writer(csvoutput, lineterminator='\n')
        reader = csv.reader(csvinput)

        all = []
        row = next(reader)
        
#Added two new columns to define payment category based on dollar amount and Record Type to distinguish between Denials and Rejections when merged. 
	row.append('Payment Category')
        row.append('Record Type')
	all.append(row)

# Segregating records based on the dollar amount. Categories are chosen as Ranks. Higher the rank more expensive the claims are. 
        for row in reader:

		if abs(float(row[8])) < 100:
			row.append('1')
			row.append('R')
             		all.append(row)
	   
		elif 100 <= abs(float(row[8])) < 500:
			row.append('2')
			row.append('R')
			all.append(row)

		elif 500 <= abs(float(row[8])) < 1000:
			row.append('3')
			row.append('R')
			all.append(row)	
		
		elif 1000 <= abs(float(row[8])) < 5000:
             		row.append('4')
			row.append('R')
			all.append(row)

		elif 5000 <= abs(float(row[8])) < 10000:
             		row.append('5')
			row.append('R')
			all.append(row)

		elif 10000 <= abs(float(row[8])) < 50000:
             		row.append('6')
			row.append('R')
			all.append(row)

		elif 50000 <= abs(float(row[8])) < 100000:
                        row.append('7')
			row.append('R')
			all.append(row)	 

		
		elif abs(float(row[8])) > 100000:
                        row.append('8')
                        row.append('R')
			all.append(row)
	

        writer.writerows(all)
